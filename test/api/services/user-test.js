'use strict';

const {assert} = require('chai');
var request = require('supertest');
var server = require('../../../app');

describe('controllers', function () {
  describe('User', function () {
    describe('POST /users && GET /users/{user_id}', function () {
      it('Create a user, then retrieve user to confirm details are correct', function (done) {
        let payload = {
          username: 'john.doe',
          first_name: 'john',
          last_name: 'doe'
        };

        request(server)
          .post('/api/users')
          .set('Accept', 'application/json')
          .send(payload)
          .expect(201)
          .end(function (err, res) {
            if (err) { return done(err); }

            let id = res.body['_id'];
            request(server)
              .get('/api/users/' + id)
              .set('Accept', 'application/json')
              .expect('Content-Type', 'application/json')
              .expect(200)
              .end(function (err, response) {
                if (err) { return done(err); }

                let expected = {
                  username: response.body.username,
                  first_name: response.body.first_name,
                  last_name: response.body.last_name
                };

                assert.deepEqual(payload, expected, 'The user we retrieved is not what we created');
                done();
              });
          });
      });
    });
    describe('PUT /users/{user_id}', function () {
      it('Create a user, then update the user details', function (done) {
        let payload = {
          username: 'chuck.norris',
          first_name: 'chuck',
          last_name: 'norris'
        };

        request(server)
          .post('/api/users')
          .set('Accept', 'application/json')
          .send(payload)
          .expect(201)
          .end(function (err, res) {
            if (err) { return done(err); }

            let id = res.body['_id'];
            let updatedUser = {
              username: 'jacky.chan',
              first_name: 'jacky',
              last_name: 'chan'
            };
            request(server)
              .put('/api/users/' + id)
              .set('Accept', 'application/json')
              .send(updatedUser)
              .expect(200)
              .end(function (err, response) {
                if (err) { return done(err); }

                let expected = {
                  username: response.body.username,
                  first_name: response.body.first_name,
                  last_name: response.body.last_name
                };

                assert.deepEqual(updatedUser, expected, 'The user that we updated did not have the updated details');
                done();
              });
          });
      });
    });
    describe('GET /users', function () {
      it('Get a list of all users', function (done) {
        request(server)
          .get('/api/users')
          .set('Accept', 'application/json')
          .expect(200)
          .end(function (err, res) {
            if (err) { return done(err); }

            assert(Array.isArray(res.body), 'Get list should return an array');
            assert.isAbove(res.body.length, 1, 'We expected get list to alteast return 1 user.');
            done();
          });
      });
    });
  });
});
