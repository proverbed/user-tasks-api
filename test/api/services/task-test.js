'use strict';

const {assert} = require('chai');
const moment = require('moment');
var request = require('supertest');
var server = require('../../../app');

/**
 * Creates a new user
 *
 * @param {*} done
 */
function createUser(done) {
  let payload = {
    username: 'john.doe',
    first_name: 'john',
    last_name: 'doe'
  };
  request(server)
    .post('/api/users')
    .set('Accept', 'application/json')
    .send(payload)
    .expect(201)
    .end(function (err, res) {
      if (err) { return done(err); }
      done(null, res.body['_id']);
    });
}

describe('controllers', function () {
  describe('Task', function () {
    describe('GET /users/{user_id}/tasks', function () {
      let user_id;
      before(function (done) {
        createUser((err, response) => {
          if (err) { return done(err); }
          user_id = response;
          done();
        });
      });
      it('Retrieve tasks on a user with no tasks', function (done) {
        request(server)
          .get('/api/users/' + user_id + '/tasks')
          .set('Accept', 'application/json')
          .expect(204)
          .end(function (err) {
            if (err) { return done(err); }
            done();
          });
      });
      it('Create tasks on a new user, then get all tasks', function (done) {
        let task = {
          name: 'drycleaning',
          description: 'Pick up drycleaning',
          date_time: '2017-08-05 12:15:00'
        };

        request(server)
          .post('/api/users/' + user_id + '/tasks')
          .set('Accept', 'application/json')
          .send(task)
          .expect(201)
          .end(function (err) {
            if (err) { return done(err); }

            request(server)
              .get('/api/users/' + user_id + '/tasks')
              .set('Accept', 'application/json')
              .expect(200)
              .end(function (err, res) {
                if (err) { return done(err); }

                assert(res.body.length === 1, 'We expected only one task.');
                done();
              });
          });
      });
    });
    describe('POST /users/{user_id}/tasks && GET /users/{user_id}/tasks/{task_id}', function () {
      let user_id;
      let task_id;
      before(function (done) {
        createUser((err, response) => {
          if (err) { return done(err); }
          user_id = response;
          done();
        });
      });
      it('Create a task, then retrieve the task to confirm details are correct', function (done) {
        let task = {
          name: 'drycleaning',
          description: 'Pick up drycleaning',
          date_time: '2017-08-05 12:15:00'
        };

        request(server)
          .post('/api/users/' + user_id + '/tasks')
          .set('Accept', 'application/json')
          .send(task)
          .expect(201)
          .end(function (err, res) {
            if (err) { return done(err); }

            task_id = res.body['_id'];
            request(server)
              .get('/api/users/' + user_id + '/tasks/' + task_id)
              .set('Accept', 'application/json')
              .expect('Content-Type', 'application/json')
              .expect(200)
              .end(function (err, response) {
                if (err) { return done(err); }

                let expected = {
                  name: response.body.name,
                  description: response.body.description,
                  date_time: response.body.date_time
                };
                task.date_time = moment(task.date_time).toISOString();
                assert.deepEqual(task, expected, 'The task we retrieved is not what we created');
                done();
              });
          });
      });
      describe('PUT /users/{user_id}/tasks/{task_id}', function () {
        it('Update a task, then retrieve the task to confirm details are correct', function (done) {
          let task = {
            name: 'Dry Cleaning Updated',
            description: 'Pick up Dry Cleaning',
            date_time: '2017-08-15 12:15:00'
          };
          request(server)
            .put('/api/users/' + user_id + '/tasks/' + task_id)
            .set('Accept', 'application/json')
            .send(task)
            .expect(200)
            .end(function (err, res) {
              if (err) { return done(err); }

              let id = res.body['_id'];
              request(server)
                .get('/api/users/' + user_id + '/tasks/' + id)
                .set('Accept', 'application/json')
                .expect('Content-Type', 'application/json')
                .expect(200)
                .end(function (err, response) {
                  if (err) { return done(err); }

                  let expected = {
                    name: response.body.name,
                    description: response.body.description,
                    date_time: response.body.date_time
                  };
                  task.date_time = moment(task.date_time).toISOString();
                  assert.deepEqual(task, expected, 'The task we retrieved is not what we created');
                  done();
                });
            });
        });
      });
      describe('DELETE /users/{user_id}/tasks/{task_id}', function () {
        it('Delete a task, then confirm the task has been deleted', function (done) {
          request(server)
            .delete('/api/users/' + user_id + '/tasks/' + task_id)
            .set('Accept', 'application/json')
            .expect(200)
            .end(function (err, res) {
              if (err) { return done(err); }

              let id = res.body['_id'];
              request(server)
                .get('/api/users/' + user_id + '/tasks/' + id)
                .set('Accept', 'application/json')
                .expect('Content-Type', 'application/json')
                .expect(204)
                .end(function (err) {
                  if (err) { return done(err); }
                  done();
                });
            });
        });
      });
    });
  });
});
