# User Tasks

This is a simple NodeJS restful application that manages users and tasks for those users.

The data will be persisted to a MongoDB.

## Swagger Docs

To see the routes that this application exposes, see the swagger docs details logged to the console, when the application starts up.

## Configure MongoDB

To configure the MongoDB to use update: `config/default.json` and `config/testing.json` for the test database.

## Launch Application

To launch the application and see on which ports the api is serving:

```sh
npm start
```

## Test

To run the tests for this application:

```sh
npm test
```