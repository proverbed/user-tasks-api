'use strict';

const UserService = require('../../api/services/UserService');

/**
 * Create a new user
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.createUser = function createUser(req, res, next) {
  let userService = new UserService();
  userService.createUser(req.swagger.params, res, next);
};

/**
 * Update user details
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.updateUserDetails = function updateUserDetails(req, res, next) {
  let userService = new UserService();
  userService.updateUserDetails(req.swagger.params, res, next);
};

/**
 * Retrieve user details
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.retrieveUserDetails = function retrieveUserDetails(req, res, next) {
  let userService = new UserService();
  userService.retrieveUserDetails(req.swagger.params, res, next);
};

/**
 * List all users details
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.retrieveUserListing = function retrieveUserListing(req, res, next) {
  let userService = new UserService();
  userService.retrieveUserListing(req.swagger.params, res, next);
};