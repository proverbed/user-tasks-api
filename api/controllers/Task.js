'use strict';

const TaskService = require('../../api/services/TaskService');

/**
 * Create a task for a user
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.createUserTask = function createUserTask(req, res, next) {
  let taskService = new TaskService();
  taskService.createUserTask(req.swagger.params, res, next);
};

/**
 * List all tasks for a user
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.retrieveUserTaskListing = function retrieveUserTaskListing(req, res, next) {
  let taskService = new TaskService();
  taskService.retrieveUserTaskListing(req.swagger.params, res, next);
};

/**
 * Get user task details
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.retrieveUserTaskDetails = function retrieveUserTaskDetails(req, res, next) {
  let taskService = new TaskService();
  taskService.retrieveUserTaskDetails(req.swagger.params, res, next);
};

/**
 * Update user task details
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.updateUserTaskDetails = function updateUserTaskDetails(req, res, next) {
  let taskService = new TaskService();
  taskService.updateUserTaskDetails(req.swagger.params, res, next);
};

/**
 * Delete a user task
 *
 * @param {ClientRequest} req - The http request object
 * @param {IncomingMessage} res - The http response object
 * @param {function} next The callback used to pass control to the next action/middleware
 */
module.exports.deleteUserTask = function deleteUserTask(req, res, next) {
  let taskService = new TaskService();
  taskService.deleteUserTask(req.swagger.params, res, next);
};