'use strict';

// This file is used to export the model Schemas
const User = require('./User');
const Task = require('./Task');

// Export all the schema for ease of access
module.exports = {
  User: User,
  Task: Task
};