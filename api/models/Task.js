'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let TaskSchema = new Schema(
  {
    'name': {
      type: String,
      required: true
    },
    'description': {
      type: String,
      required: true
    },
    'user_id': {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    'date_time': {
      type: Date,
      required: true
    }
  },
  {
    timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'},
    collection: 'Task'
  }
);

/**
 * Defines the schema for a Task
 *
 * @author Dmitri De Klerk <dmitriwarren@gmail.com>
 * @since  21 Sept 2017
 *
 * @module Task
 */
module.exports = mongoose.model('Task', TaskSchema);