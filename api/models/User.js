'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema(
  {
    'username': {
      type: String,
      required: true
    },
    'first_name': {
      type: String,
      required: true
    },
    'last_name': {
      type: String,
      required: true
    }
  },
  {
    timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'},
    collection: 'User'
  }
);

/**
 * Defines the schema for a User
 *
 * @author Dmitri De Klerk <dmitriwarren@gmail.com>
 * @since  21 Sept 2017
 *
 * @module User
 */
module.exports = mongoose.model('User', UserSchema);