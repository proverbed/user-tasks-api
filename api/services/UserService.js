'use strict';

const {User} = require('../models/');

class UserService {

  /**
   * Create a new User
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  createUser(swaggerParams, res, next) {
    let createUserPayload = swaggerParams.createUser.value;
    let user = new User(createUserPayload);

    user.save((error, response) => {
      if (error) {
        return next(error);
      }

      res.statusCode = 201;
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(response));
    });
  }

  /**
   * Update user details
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  updateUserDetails(swaggerParams, res, next) {
    let id = swaggerParams.user_id.value;
    let updateUserPayload = swaggerParams.updateUser.value;

    User.findByIdAndUpdate(
      id, // Search Filter
      updateUserPayload, // update
      {new: true}, // options, explicitly set new to true, to get new version of doc
      (error, response) => {
        if (error) {
          return next(error);
        }

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(response));
      });
  }

  /**
   * Retrieve user details
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  retrieveUserDetails(swaggerParams, res, next) {
    let id = swaggerParams.user_id.value;

    User.findById(
      id, // Search Filter
      null, // Return all fields
      null, // options
      (error, response) => {
        if (error) {
          return next(error);
        }

        if (!response || response.length == 0) {
          res.statusCode = 204;
        } else {
          res.statusCode = 200;
        }

        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(response));
      });
  }

  /**
   * List all user details
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  retrieveUserListing(swaggerParams, res, next) {

    User.find(
      {}, // Search Filter
      null, // Return all fields
      null, // options
      (error, response) => {
        if (error) {
          return next(error);
        }

        if (!response || response.length == 0) {
          res.statusCode = 204;
        } else {
          res.statusCode = 200;
        }

        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(response));
      });
  }

}

module.exports = UserService;