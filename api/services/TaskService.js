'use strict';

const {User, Task} = require('../models/');

class TaskService {

  /**
   * Create a task for a user
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  createUserTask(swaggerParams, res, next) {
    let createUserTaskPayload = swaggerParams.createUserTask.value;
    let user_id = swaggerParams.user_id.value;

    User.findById(
      user_id, // Search Filter
      null, // Return all fields
      null, // options
      (error, response) => {
        if (error) {
          return next(error);
        }

        res.setHeader('Content-Type', 'application/json');
        if (!response || response.length == 0) {
          let resourceNotFound = {
            message: 'User with id [' + user_id + '] was not found'
          };
          res.statusCode = 404;
          return next(resourceNotFound);
        } else {
          // The user_id is valid insert task for user
          createUserTaskPayload.user_id = user_id;
          let task = new Task(createUserTaskPayload);
          task.save((error, response) => {
            if (error) {
              return next(error);
            }

            res.statusCode = 201;
            res.end(JSON.stringify(response));
          });
        }
      });
  }

  /**
   * List all tasks for a user
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  retrieveUserTaskListing(swaggerParams, res, next) {
    let user_id = swaggerParams.user_id.value;
    let query = {
      user_id: user_id
    };
    Task.find(
      query, // Search Filter
      null, // Return all fields
      null, // options
      (error, response) => {
        if (error) {
          return next(error);
        }

        if (!response || response.length == 0) {
          res.statusCode = 204;
        } else {
          res.statusCode = 200;
        }

        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(response));
      });
  }

  /**
   * Get user task details
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  retrieveUserTaskDetails(swaggerParams, res, next) {
    let user_id = swaggerParams.user_id.value;
    let task_id = swaggerParams.task_id.value;
    let query = {
      user_id: user_id,
      _id: task_id
    };
    Task.findOne(
      query, // Search Filter
      null, // Return all fields
      null, // options
      (error, response) => {
        if (error) {
          return next(error);
        }

        if (!response || response.length == 0) {
          res.statusCode = 204;
        } else {
          res.statusCode = 200;
        }

        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(response));
      });
  }

  /**
   * Update user task details
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  updateUserTaskDetails(swaggerParams, res, next) {
    let user_id = swaggerParams.user_id.value;
    let task_id = swaggerParams.task_id.value;
    let updateUserTaskPayload = swaggerParams.updateUserTask.value;

    User.findById(
      user_id, // Search Filter
      null, // Return all fields
      null, // options
      (error, response) => {
        if (error) {
          return next(error);
        }

        res.setHeader('Content-Type', 'application/json');
        if (!response || response.length == 0) {
          let resourceNotFound = {
            message: 'User with id [' + user_id + '] was not found'
          };
          res.statusCode = 404;
          return next(resourceNotFound);
        } else {
          // The user_id is valid
          Task.findByIdAndUpdate(
            task_id, // Search Filter
            updateUserTaskPayload, // update
            {new: true}, // options, explicitly set new to true, to get new version of doc
            (error, response) => {
              if (error) {
                return next(error);
              }

              res.statusCode = 200;
              res.setHeader('Content-Type', 'application/json');
              res.end(JSON.stringify(response));
            });
        }
      });
  }

  /**
   * Delete a user task
   *
   * @param {object} args - The request arguments passed in from the controller
   * @param {IncomingMessage} res - The http response object
   * @param {function} next - The callback used to pass control to the next action/middleware
   *
   * @return void
   */
  deleteUserTask(swaggerParams, res, next) {
    let user_id = swaggerParams.user_id.value;
    let task_id = swaggerParams.task_id.value;

    User.findById(
      user_id, // Search Filter
      null, // Return all fields
      null, // options
      (error, response) => {
        if (error) {
          return next(error);
        }

        res.setHeader('Content-Type', 'application/json');
        if (!response || response.length == 0) {
          let resourceNotFound = {
            message: 'User with id [' + user_id + '] was not found'
          };
          res.statusCode = 404;
          return next(resourceNotFound);
        } else {
          // The user_id is valid
          Task.findByIdAndRemove(
            task_id, // Search Filter
            (error, response) => {
              if (error) {
                return next(error);
              }

              res.statusCode = 200;
              res.setHeader('Content-Type', 'application/json');
              res.end(JSON.stringify(response));
            });
        }
      });
  }

}

module.exports = TaskService;