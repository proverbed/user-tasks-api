# Configuration Files

Place configuration files in this directory.

The configuration file that will be used will the the one specified as the environment value: `NODE_ENV`

For more information see: <https://www.npmjs.com/package/config>